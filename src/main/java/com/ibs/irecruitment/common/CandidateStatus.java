package com.ibs.irecruitment.common;

public enum CandidateStatus {
    SHORTLIST("shortlist"), WAITINGLIST("waitinglist"), SELECTED("selected"), REJECTED("rejected");

    private String status;

    CandidateStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
