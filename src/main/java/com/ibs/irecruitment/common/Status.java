package com.ibs.irecruitment.common;

public enum Status {

    ACTIVE(1, "Active"), IN_ACTIVE(2, "InActive"), OPEN(3, "Open"), SUBMITTED(0, "Submitted");

    private final Integer key;
    private final String value;

    Status(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    public Integer getKey() {
        return key;
    }
    public String getValue() {
        return value;
    }
}