package com.ibs.irecruitment.service.interfaces;

import com.ibs.irecruitment.model.LocationModel;

import java.util.List;

public interface LocationService {
    List<LocationModel> findAllLocations();
}
