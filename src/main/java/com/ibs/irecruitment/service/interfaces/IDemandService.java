package com.ibs.irecruitment.service.interfaces;

import com.ibs.irecruitment.model.Demand;
import com.ibs.irecruitment.model.DemandGroup;
import com.ibs.irecruitment.model.RecruiterModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IDemandService {
    void parseDemandExcel(MultipartFile file);

    List<Demand> getAllDemands();

    RecruiterModel assignDemandToRecruiter(RecruiterModel recruiterModel);

    List<RecruiterModel> createRecruiter(List<RecruiterModel> recruiters);

    List<RecruiterModel> getAllRecruiters();

    List<RecruiterModel> getAllRecruiterWithDemand();

    DemandGroup groupDemands(DemandGroup demandGroup);

    List<DemandGroup> getGroupedDemands(String status);

    List<Demand> updateDemandStatus(List<Demand> demands);

    List<Demand> updateHiringStatus(List<Demand> demands);

    DemandGroup updateGroupDemands(DemandGroup demandGroup);

    List<Demand> getAllActiveDemands();
}
