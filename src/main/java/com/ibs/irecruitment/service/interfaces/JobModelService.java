package com.ibs.irecruitment.service.interfaces;

import com.ibs.irecruitment.model.JobModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface JobModelService {
    JobModel createNewJobPost(JobModel jobModel);
    JobModel updateJobPost(JobModel jobModel);
    List<JobModel> getAllJobOpening();
    List<JobModel> bulkInsertJobPostings(MultipartFile file);
}
