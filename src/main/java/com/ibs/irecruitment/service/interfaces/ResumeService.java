package com.ibs.irecruitment.service.interfaces;

import com.ibs.irecruitment.model.CandidateModel;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ResumeService {
    List<CandidateModel> parseResume(List<MultipartFile> multipartFiles);
}
