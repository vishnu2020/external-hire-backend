package com.ibs.irecruitment.service.interfaces;

import com.ibs.irecruitment.model.RecruiterModel;

import java.util.List;

public interface RecruiterService {
    List<RecruiterModel> findAllRecruiters();
}
