package com.ibs.irecruitment.service.interfaces;

import com.ibs.irecruitment.model.CandidateModel;
import com.ibs.irecruitment.repository.CandidateRepo;
import com.ibs.irecruitment.model.Demand;

import java.util.List;

public interface CandidateService {
    List<CandidateModel> getAllCandidate(String status, List<String> experience, List<String> skills, List<String> qualification);
    List<CandidateModel> getAllCandidateByJobId(int jobId);
    CandidateModel saveCandidate(CandidateModel applyRequestModel);
    List<CandidateModel> saveCandidateList(List<CandidateModel> candidateModel);

    // dummy method
    Object updateCandidatesStatus(String status);

    CandidateModel mapCandidateToJob(Integer jobId, Integer candidateId);

    CandidateModel mapCandidateWithDemands(CandidateModel candidateModel);

    byte[] exportCandidateDemandExcel(List<Demand> demands);
}
