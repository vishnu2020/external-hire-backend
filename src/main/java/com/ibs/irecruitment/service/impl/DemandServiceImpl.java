package com.ibs.irecruitment.service.impl;

import com.ibs.irecruitment.common.Status;
import com.ibs.irecruitment.model.Demand;
import com.ibs.irecruitment.model.DemandGroup;
import com.ibs.irecruitment.model.RecruiterModel;
import com.ibs.irecruitment.repository.DemandGroupRepo;
import com.ibs.irecruitment.repository.DemandRepo;
import com.ibs.irecruitment.repository.TempRecruiterRepo;
import com.ibs.irecruitment.service.interfaces.IDemandService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DemandServiceImpl implements IDemandService {

    @Autowired
    private DemandRepo demandRepo;

    @Autowired
    private TempRecruiterRepo tempRecruiterRepo;

    @Autowired
    private DemandGroupRepo demandGroupRepo;

    @Override
    public void parseDemandExcel(MultipartFile file) {
        try {
            InputStream inputStream =  new BufferedInputStream(file.getInputStream());
            Workbook workbook = new XSSFWorkbook(inputStream);

            Sheet sheet = workbook.getSheetAt(0);

            Row header = sheet.getRow(0);
            sheet.removeRow(header);
            Date fileUploadDate = new Date();
            int i = 0;
            for (Row row : sheet) {
                Demand demand = new Demand();
                demand.setRequestId(row.getCell(0) !=null ? row.getCell(0).getStringCellValue() : "" );
                demand.setRole(row.getCell(1) !=null ? row.getCell(1).getStringCellValue() : "" );
                demand.setAccountName(row.getCell(2) !=null ? row.getCell(2).getStringCellValue() : "" );
                demand.setOpportunity(row.getCell(3) !=null ? row.getCell(3).getStringCellValue() : "" );
                demand.setBoProject(row.getCell(4) !=null ? row.getCell(4).getStringCellValue() : "" );
                demand.setFulfilledRole(row.getCell(5) !=null ? row.getCell(5).getStringCellValue() : "" );
                demand.setExperience(row.getCell(6) !=null ? row.getCell(6).getStringCellValue() : "" );
                demand.setLocation(row.getCell(7) !=null ? row.getCell(7).getStringCellValue() : "" );
                demand.setStatus(row.getCell(8) !=null ? Status.valueOf(row.getCell(8).getStringCellValue().toUpperCase(Locale.ENGLISH)).getValue() : Status.OPEN.getValue() );
                demand.setAssigned(row.getCell(9) !=null
                        ? row.getCell(9).getStringCellValue().trim() == "-"
                        : false );
                demand.setStartDate(row.getCell(10) !=null ? row.getCell(10).getDateCellValue() : null );
                demand.setEndDate(row.getCell(11) !=null ? row.getCell(11).getDateCellValue() : null );
                demand.setFulfilment(row.getCell(12) !=null ? row.getCell(12).getStringCellValue() : "" );
                demand.setClientInterview(row.getCell(13) !=null ? row.getCell(13).getStringCellValue() : "" );
                demand.setJobDescription(row.getCell(14) !=null ? row.getCell(14).getStringCellValue() : "" );
                demand.setExtended(row.getCell(15) !=null ? row.getCell(15).getStringCellValue().toLowerCase(Locale.ENGLISH) == "yes" ? true : false : false );
                demand.setRequestedOn(row.getCell(16).getDateCellValue());
                demand.setRequestCategory(row.getCell(17) !=null ? row.getCell(17).getStringCellValue() : "" );
                demand.setCurrentProbability(row.getCell(18) !=null ? row.getCell(18).getStringCellValue() : "" );
                demand.setRevenueForecast(row.getCell(19) !=null ? row.getCell(19).getStringCellValue() : "" );
                demand.setRequestedBy(row.getCell(20) !=null ? row.getCell(20).getStringCellValue() : "" );
                demand.setRequesterId(row.getCell(21) !=null ? row.getCell(21).getStringCellValue() : "" );
                demand.setDetails(row.getCell(22) !=null ? row.getCell(22).getStringCellValue() : "" );
                demand.setSkills(row.getCell(23) !=null ? row.getCell(23).getStringCellValue() : "" );
                demand.setCancelledDate(row.getCell(24) !=null ? row.getCell(24).getDateCellValue() : null );
                demand.setFulfilledDate(row.getCell(25) !=null ? row.getCell(25).getDateCellValue() : null );
                demand.setApprovedDate(row.getCell(26) !=null ? row.getCell(26).getDateCellValue() : null );
                demand.setMappedDate(row.getCell(27) !=null ? row.getCell(27).getDateCellValue() : null );
                demand.setbFMApprovalDate(row.getCell(28) != null ? row.getCell(28).getDateCellValue() : null);
                demand.settMTStatus(row.getCell(29) !=null ? row.getCell(29).getStringCellValue() : "" );
                demand.setCandidate(row.getCell(30) != null ? (int) row.getCell(30).getNumericCellValue() : -1);
                demand.setJlMapping(row.getCell(31) !=null ? row.getCell(31).getStringCellValue() : "" );
                demand.setStream(row.getCell(32) !=null ? row.getCell(32).getStringCellValue() : "" );
                demand.setIvVDomain(row.getCell(33) !=null ? row.getCell(33).getStringCellValue() : "" );
                demand.setCategory(row.getCell(34) !=null ? row.getCell(34).getStringCellValue() : "" );
                demand.setMonth(row.getCell(35) !=null ? row.getCell(35).getStringCellValue() : "" );
                demand.setFulfilmentStream(row.getCell(36) !=null ? row.getCell(36).getStringCellValue() : "" );
                demand.setSourceForInternal(row.getCell(37) !=null ? row.getCell(37).getStringCellValue() : "" );
                demand.setRemarks(row.getCell(38) !=null ? row.getCell(38).getStringCellValue() : "" );
                demand.setUploadedOn(fileUploadDate);
                demandRepo.save(demand);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public List<Demand> getAllDemands() {
        List<Demand> demands = demandRepo.findByAssigned(false);
        demands.forEach(demand -> demand.setCandidateModel(null));
        return demands;
    }

    @Override
    public RecruiterModel assignDemandToRecruiter(RecruiterModel recruiterModel) {
        recruiterModel.getDemands().forEach(demand -> demand.setAssigned(true));
        List<Demand> newDemands = demandRepo.saveAll(recruiterModel.getDemands());
        RecruiterModel existingModel = tempRecruiterRepo.findById(recruiterModel.getId()).get();
        existingModel.getDemands().addAll(newDemands);
        return tempRecruiterRepo.save(existingModel);
    }

    @Override
    public List<RecruiterModel> createRecruiter(List<RecruiterModel> recruiters) {
        Date currentDate = new Date();
        recruiters.forEach(recruiter -> {
            recruiter.setCreatedOn(currentDate);
            recruiter.setUpdatedOn(currentDate);
        });
        return tempRecruiterRepo.saveAll(recruiters);
    }

    @Override
    public List<RecruiterModel> getAllRecruiters() {
        List<Object[]> recruiters = tempRecruiterRepo.findRecruiterWithoutDemand();
        return recruiters.stream().map(recruiter ->
                new RecruiterModel(Integer.parseInt(recruiter[0].toString()),recruiter[1].toString(),recruiter[2].toString()))
                .collect(Collectors.toList());
    }

    @Override
    public List<RecruiterModel> getAllRecruiterWithDemand() {
        return tempRecruiterRepo.findAll();
    }

    @Override
    public DemandGroup groupDemands(DemandGroup demandGroup) {
        Date currentDate = new Date();
        demandGroup.setCreatedDate(currentDate);
        demandGroup.setUpdatedOn(currentDate);
        demandGroup.setStatus("Active");
        createNameForJDGroup(demandGroup);
//        demandGroup.getDemands().forEach(demand -> demand.setStatus(Status.ACTIVE.getValue()));
        List<Demand> demands = demandGroup.getDemands();
        demands.forEach(demand -> {
            demand.setGrouped(true);
            demand.setHiringStatus("ongoing");
        });
        demandRepo.saveAll(demands);
        demandGroup.setHiringStatus("ongoing");
        return demandGroupRepo.save(demandGroup);
    }

    @Override
    public List<DemandGroup> getGroupedDemands(String status) {
        List<DemandGroup> demandGroups = demandGroupRepo.findByStatus(status);
        demandGroups.forEach(demandGroup -> demandGroup.getDemands().forEach(demand -> demand.setCandidateModel(null)));
        return demandGroups;
    }

    @Override
    public List<Demand> updateDemandStatus(List<Demand> demands) {
        demands.forEach(demand -> demand.setHiringStatus("ongoing"));
        return demandRepo.saveAll(demands);
    }

    @Override
    public List<Demand> updateHiringStatus(List<Demand> demands) {
        return demandRepo.saveAll(demands);
    }

    @Override
    public DemandGroup updateGroupDemands(DemandGroup demandGroup) {
        List<Demand> demands = demandGroup.getDemands();
        DemandGroup existingDemandGroup = demandGroupRepo.findById(demandGroup.getJdGroupId()).get();
        Set<Integer> ids = demands.stream()
                .map(Demand::getId)
                .collect(Collectors.toSet());
        List<Demand> removedDemands = existingDemandGroup.getDemands().stream()
                .filter(demand -> !ids.contains(demand.getId()))
                .collect(Collectors.toList());
        removedDemands.forEach(demand -> demand.setGrouped(false));
        demandRepo.saveAll(removedDemands);
        demands = demandRepo.saveAll(demands);
        demandGroup.setDemands(demands);
        return demandGroupRepo.save(demandGroup);
    }

    @Override
    public List<Demand> getAllActiveDemands() {
        List<Demand> demands = demandRepo.findByStatus("Active");
        demands = demands.stream().filter(demand -> demand.getCandidateModel().size() != 0).collect(Collectors.toList());
        demands.forEach(demand -> {
            demand.getCandidateModel().forEach(candidate -> {
                candidate.setDemands(null);
                candidate.setJobModel(null);
            });
        });
        return demands;
    }

    private void createNameForJDGroup(DemandGroup demandGroup) {
        long totalGroups = demandGroupRepo.count();
        demandGroup.setJdGroupName("JDGRP-"+ totalGroups++);
    }
}
