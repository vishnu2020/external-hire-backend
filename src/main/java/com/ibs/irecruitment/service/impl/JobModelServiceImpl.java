package com.ibs.irecruitment.service.impl;

import com.ibs.irecruitment.model.JobModel;
import com.ibs.irecruitment.repository.JobModelRepo;
import com.ibs.irecruitment.repository.LocationRepo;
import com.ibs.irecruitment.service.interfaces.JobModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JobModelServiceImpl implements JobModelService {

    @Autowired
    private JobModelRepo jobModelRepo;

    @Autowired
    private LocationRepo locationRepo;

    @Override
    public JobModel createNewJobPost(JobModel jobPosting) {
        Date currentDate = new Date();
        jobPosting.setUpdatedOn(currentDate);
        jobPosting.setCreatedOn(currentDate);
        return jobModelRepo.save(jobPosting);
    }

    @Override
    public JobModel updateJobPost(JobModel jobModel) {
        Date currentDate = new Date();
        jobModel.setUpdatedOn(currentDate);
        return jobModelRepo.save(jobModel);
    }

    @Override
    public List<JobModel> getAllJobOpening() {
        List<JobModel> jobModels = (List<JobModel>) jobModelRepo.findAll();
        jobModels.forEach(jobModel -> jobModel.setCandidateModels(jobModel.getCandidateModels().stream().map(candidateModel -> {
            candidateModel.setJobModel(null);
            candidateModel.getDemands().forEach(demand -> demand.setCandidateModel(null));
            return  candidateModel;
        }).collect(Collectors.toList())));
        return jobModels;
    }


    @Override
    public List<JobModel> bulkInsertJobPostings(MultipartFile file) {
        return null;
    }

}
