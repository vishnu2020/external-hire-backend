package com.ibs.irecruitment.service.impl;

import com.ibs.irecruitment.model.CandidateModel;
import com.ibs.irecruitment.repository.CandidateRepo;
import com.ibs.irecruitment.service.interfaces.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ResumeServiceImpl implements ResumeService {

    @Value( "${resumeParser.url.base}" )
    private String parserBaseUrl;

    @Value("${resumeParser.url.bulkParse}")
    private String bulkParseUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CandidateRepo candidateRepo;


    @Override
    public List<CandidateModel> parseResume(List<MultipartFile> multipartFiles) {
        final String baseUrl = parserBaseUrl+bulkParseUrl;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);

            MultiValueMap<String, Object> map= new LinkedMultiValueMap<>();
            multipartFiles.forEach(file -> {
                try {
                    map.add("resumes", new MultipartInputStreamFileResource(file.getInputStream(), file.getOriginalFilename()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
            ResponseEntity<List<CandidateModel>> response = restTemplate.exchange(baseUrl, HttpMethod.POST, request, new ParameterizedTypeReference<List<CandidateModel>>() {});
            List<CandidateModel> candidateModels = response.getBody();
            Date date = new Date();
            candidateModels.forEach(candidateModel -> {
                candidateModel.setStatus("waitinglist");
                candidateModel.setAppliedOn(date);
            });
            return candidateRepo.saveAll(candidateModels);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}

class MultipartInputStreamFileResource extends InputStreamResource {

    private final String filename;

    MultipartInputStreamFileResource(InputStream inputStream, String filename) {
        super(inputStream);
        this.filename = filename;
    }

    @Override
    public String getFilename() {
        return this.filename;
    }

    @Override
    public long contentLength() throws IOException {
        return -1; // we do not want to generally read the whole stream into memory ...
    }
}