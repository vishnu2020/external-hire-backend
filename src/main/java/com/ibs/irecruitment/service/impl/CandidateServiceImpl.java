package com.ibs.irecruitment.service.impl;

import com.ibs.irecruitment.model.CandidateModel;
import com.ibs.irecruitment.model.JobModel;
import com.ibs.irecruitment.repository.CandidateRepo;
import com.ibs.irecruitment.repository.DemandRepo;
import com.ibs.irecruitment.repository.JobModelRepo;
import com.ibs.irecruitment.service.interfaces.CandidateService;
import com.ibs.irecruitment.utilities.ModifyXMLFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import com.ibs.irecruitment.model.Demand;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;

@Service
public class CandidateServiceImpl implements CandidateService {

    @Autowired
    private CandidateRepo candidateRepo;

    @Autowired
    private JobModelRepo jobModelRepo;

    @Autowired
    private DemandRepo demandRepo;


    @Autowired
    private HttpServletRequest request;

    @Override
    public List<CandidateModel> getAllCandidate(String status, List<String> experience, List<String> skills, List<String> qualification) {
        return candidateRepo.getCandidateByApplyingFilter(status, experience, skills, qualification);
    }

    @Override
    public List<CandidateModel> getAllCandidateByJobId(int jobId) {
        return (List<CandidateModel>) candidateRepo.findByJobId(jobId);
    }


    @Override
    public CandidateModel saveCandidate(CandidateModel candidateModel) {
        Date currentDate = new Date();
        candidateModel.setAppliedOn(currentDate);
        if (!candidateModel.getResume().isEmpty()) {
            try {
                String uploadsDir = "/file/";
                String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
                if (!new File(realPathtoUploads).exists()) {
                    new File(realPathtoUploads).mkdir();
                }
                List<CandidateModel> lastid = candidateRepo.findLastByOrderByCandidateIdDesc();
                String lastId =lastid.get(0).getFileName();
                String currentVallue=lastId.substring(2,(lastId.length()-10));
                Date d=new Date();
                int year=d.getYear();
                int currentYear=year+2000;
                String editYear = Integer.toString(currentYear);
                String UpdateYear= editYear.substring(2);
                int currentYear2=Integer.parseInt(UpdateYear)+1;
                String updatedValue=lastId.substring(2,(lastId.length()-10));
                int print1=Integer.parseInt(updatedValue)+1;
                if (currentVallue.equals(UpdateYear)){

                    String latest=lastId;
                    String splitValue=latest.substring(5,(latest.length()-5));
                    int splitValue1=Integer.parseInt(splitValue)+1;
                    String completeNewVersion="cp"+ updatedValue+"-"+splitValue1+".docx";
                    String filename = completeNewVersion
                            + candidateModel.getResume().getOriginalFilename()
                            .substring(candidateModel.getResume().getOriginalFilename().lastIndexOf("x") + 1);

                    File newName = new File(filename);
                    String filePath = realPathtoUploads +filename;
                    File dest = new File(filePath);
                    candidateModel.getResume().transferTo(dest);
                    candidateModel.setFileName(newName.toString());


                }
                else{

                    String newValue="cp"+currentYear2+"-1000.docx";
                    File newName = new File(newValue);
                    String filePath = realPathtoUploads +newName;
                    File dest = new File(filePath);
                    candidateModel.getResume().transferTo(dest);
                    candidateModel.setFileName(newName.toString());

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return candidateRepo.save(candidateModel);
    }

    @Override
    public List<CandidateModel> saveCandidateList(List<CandidateModel> candidateModel) {
        Date currentDate = new Date();
        candidateModel.forEach(candidateModel1 -> candidateModel1.setAppliedOn(currentDate));
        return StreamSupport
                .stream(candidateRepo.saveAll(candidateModel).spliterator(), false)
                .collect(Collectors.toList());
    }

    @Override
    public Object updateCandidatesStatus(String status) {
        List<CandidateModel> candidates = candidateRepo.findAll();
        candidates.forEach(candidateModel -> candidateModel.setStatus(status));
        return candidateRepo.saveAll(candidates);
    }

    @Override
    public CandidateModel mapCandidateToJob(Integer jobId, Integer candidateId) {
        CandidateModel candidateModel = candidateRepo.findById(candidateId).get();
        JobModel jobModel = jobModelRepo.findById(jobId).get();
        List<JobModel> jobModels = candidateModel.getJobModel();
        jobModels.add(jobModel);
        candidateModel.setJobModel(jobModels);
        return candidateRepo.save(candidateModel);
    }

    @Override
    public CandidateModel mapCandidateWithDemands(CandidateModel candidateModel) {
        CandidateModel persistanceCheck = candidateRepo.findById(candidateModel.getCandidateId()).get();
        List<Demand> demands = demandRepo.findAllById(candidateModel.getDemands().stream().map(demand -> demand.getId()).collect(Collectors.toList()));
        persistanceCheck.setDemands(new HashSet<com.ibs.irecruitment.model.Demand>(demands));
        return candidateRepo.save(persistanceCheck);
    }

    @Override
    public byte[] exportCandidateDemandExcel(List<Demand> demands) {

        XSSFWorkbook workbook = new XSSFWorkbook();

        // Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Demand_Candidate_Report");

        String[] headings = {"Request Id", "Role", "Account Name", "Opportunity", "BO/Project", "Fullfilled Date",
                "Experience", "Location", "Status", "Start Date", "End Date", "Job Description", "Requested On", "Request Category",
                "Requested By", "Requester Id", "Skills", "Requirement/Demand Status", "Current Status", "Total Applicants",
                "Cancelled Date", "Fulfilled Date", "Offered Date", "Declined Date", "Approved Date", "CEO Approval Date", "BFM Approval Date",
                "Candidate Name", "JL Mapping", "Stream", "Primary Skill", "Secondary Skill", "Source", "SourceName", "Source ID (if required)", "Remarks"};

        Row row = sheet.createRow(0);
        int cellIndex = 0;
        for(String heading: headings) {
            Cell cell = row.createCell(cellIndex++);
            cell.setCellValue(heading);
        }
        AtomicInteger rowCount = new AtomicInteger(1);
        demands.forEach(demand -> {
            demand.getCandidateModel().forEach(candidate -> {
                Row newRow = sheet.createRow(rowCount.getAndIncrement());
                AtomicInteger atomicInteger = new AtomicInteger(0);
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getRequestId());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getRole());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getAccountName());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getOpportunity());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getBoProject());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) demand.getFulfilledDate());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getExperience());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getLocation());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getStatus());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) demand.getStartDate());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) demand.getEndDate());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getJobDescription());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) demand.getRequestedOn());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getRequestCategory());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getRequestedBy());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getRequesterId());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getSkills());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getHiringStatus());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Integer) demand.getCandidateModel().size());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) demand.getCancelledDate());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) demand.getFulfilledDate());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) null);
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) null);
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue((Date) demand.getApprovedDate());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(candidate.getFirstName() + " " + candidate.getMiddleName() + " " + candidate.getLastName());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getJlMapping());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(demand.getStream());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue("");
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue("");
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue(candidate.getSource());
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue("");
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue("");
                newRow.createCell(atomicInteger.getAndIncrement()).setCellValue("");
            });
        });

//        try {
//            File file = new File("candidate_demand_report1.xlsx");
//            FileOutputStream out = new FileOutputStream(file);
//            System.out.println(file.getAbsolutePath());
//            workbook.write(out);
//            out.close();
//            System.out.println("gfgcontribute.xlsx written successfully on disk.");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            workbook.write(bos);
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] bytes = bos.toByteArray();

        // This data needs to be written (Object[])
//        Map<String, Object[]> data = new TreeMap<String, Object[]>();
//        data.put("1", new Object[]{ "ID", "NAME", "LASTNAME" });
//        data.put("2", new Object[]{ 1, "Pankaj", "Kumar" });
//        data.put("3", new Object[]{ 2, "Prakashni", "Yadav" });
//        data.put("4", new Object[]{ 3, "Ayan", "Mondal" });
//        data.put("5", new Object[]{ 4, "Virat", "kohli" });

        // Iterate over data and write to sheet
//        ClassPathResource resource = new ClassPathResource(
//                "reports_heading/candidate_demand_report.xml");
//        try {
//            System.out.println(resource.getFile().getAbsolutePath());
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        new ModifyXMLFile().getHeadings("");

//        Set<String> keyset = data.keySet();
//        int rownum = 0;
//        for (Demand key : demands) {
//            // this creates a new row in the sheet
//            Row row = sheet.createRow(rownum++);
//            Object[] objArr = data.get(key);
//            int cellnum = 0;
//            for (Object obj : objArr) {
//                // this line creates a cell in the next column of that row
//                Cell cell = row.createCell(cellnum++);
//                if (obj instanceof String)
//                    cell.setCellValue((String) obj);
//                else if (obj instanceof Integer)
//                    cell.setCellValue((Integer) obj);
//            }
//        }
//        try {
//            // this Writes the workbook gfgcontribute
//            FileOutputStream out = new FileOutputStream(new File("gfgcontribute.xlsx"));
//            workbook.write(out);
//            out.close();
//            System.out.println("gfgcontribute.xlsx written successfully on disk.");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return bytes;
    }

}
