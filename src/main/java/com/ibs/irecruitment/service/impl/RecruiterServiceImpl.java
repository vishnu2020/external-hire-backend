package com.ibs.irecruitment.service.impl;

import com.ibs.irecruitment.model.RecruiterModel;
import com.ibs.irecruitment.repository.RecruiterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecruiterServiceImpl implements com.ibs.irecruitment.service.interfaces.RecruiterService {

    @Autowired
    private RecruiterRepo recruiterRepo;

    @Override
    public List<RecruiterModel> findAllRecruiters(){
        return recruiterRepo.findAll();
    }
}
