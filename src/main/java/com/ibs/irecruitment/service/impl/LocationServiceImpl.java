package com.ibs.irecruitment.service.impl;

import com.ibs.irecruitment.model.LocationModel;
import com.ibs.irecruitment.repository.LocationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImpl implements com.ibs.irecruitment.service.interfaces.LocationService {

    @Autowired
    private LocationRepo locationRepo;

    @Override
    public List<LocationModel> findAllLocations(){
        return locationRepo.findAll();
    }
}
