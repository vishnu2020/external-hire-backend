package com.ibs.irecruitment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Demand {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String requestId;
    private String role;
    private String accountName;
    private String opportunity;
    private String boProject;
    private String fulfilledRole;
    private String experience;
    private String location;
    private String status;
    private boolean assigned;
    private Date startDate;
    private Date endDate;
    private String fulfilment;
    private String clientInterview;
    @Lob
    private String jobDescription;
    private boolean isExtended;
    private Date requestedOn;
    private String requestCategory;
    private String currentProbability;
    private String revenueForecast;
    private String requestedBy;
    private String requesterId;
    @Lob
    private String details;
    @Lob
    private String skills;
    private Date cancelledDate;
    private Date fulfilledDate;
    private Date approvedDate;
    private Date mappedDate;
    private Date bFMApprovalDate;
    private String tMTStatus;
    @Column(name = "candidate_id")
    private int candidate;
    private String jlMapping;
    private String stream;
    private String ivVDomain;
    private String category;
    private String month;
    private String fulfilmentStream;
    private String sourceForInternal;
    private String remarks;
    private String hiringStatus;

    private Date uploadedOn;
    private Date statusUpdatedOn;
    private String statusUpdatedBy;
    private String assignedBy;
    private boolean grouped;

    @ManyToMany(mappedBy = "demands")
    private Set<CandidateModel> candidateModel;

    public Set<CandidateModel> getCandidateModel() {
        return candidateModel;
    }

    public void setCandidateModel(Set<CandidateModel> candidateModel) {
        this.candidateModel = candidateModel;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getOpportunity() {
        return opportunity;
    }

    public void setOpportunity(String opportunity) {
        this.opportunity = opportunity;
    }

    public String getBoProject() {
        return boProject;
    }

    public void setBoProject(String boProject) {
        this.boProject = boProject;
    }

    public String getFulfilledRole() {
        return fulfilledRole;
    }

    public void setFulfilledRole(String fulfilledRole) {
        this.fulfilledRole = fulfilledRole;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public String getFulfilment() {
        return fulfilment;
    }

    public void setFulfilment(String fulfilment) {
        this.fulfilment = fulfilment;
    }

    public String getClientInterview() {
        return clientInterview;
    }

    public void setClientInterview(String clientInterview) {
        this.clientInterview = clientInterview;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getRequestCategory() {
        return requestCategory;
    }

    public void setRequestCategory(String requestCategory) {
        this.requestCategory = requestCategory;
    }

    public String getCurrentProbability() {
        return currentProbability;
    }

    public void setCurrentProbability(String currentProbability) {
        this.currentProbability = currentProbability;
    }

    public String getRevenueForecast() {
        return revenueForecast;
    }

    public void setRevenueForecast(String revenueForecast) {
        this.revenueForecast = revenueForecast;
    }

    public String getRequestedBy() {
        return requestedBy;
    }

    public void setRequestedBy(String requestedBy) {
        this.requestedBy = requestedBy;
    }

    public String getRequesterId() {
        return requesterId;
    }

    public void setRequesterId(String requesterId) {
        this.requesterId = requesterId;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String gettMTStatus() {
        return tMTStatus;
    }

    public void settMTStatus(String tMTStatus) {
        this.tMTStatus = tMTStatus;
    }

    public String getJlMapping() {
        return jlMapping;
    }

    public void setJlMapping(String jlMapping) {
        this.jlMapping = jlMapping;
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public String getIvVDomain() {
        return ivVDomain;
    }

    public void setIvVDomain(String ivVDomain) {
        this.ivVDomain = ivVDomain;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getFulfilmentStream() {
        return fulfilmentStream;
    }

    public void setFulfilmentStream(String fulfilmentStream) {
        this.fulfilmentStream = fulfilmentStream;
    }

    public String getSourceForInternal() {
        return sourceForInternal;
    }

    public void setSourceForInternal(String sourceForInternal) {
        this.sourceForInternal = sourceForInternal;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCancelledDate() {
        return cancelledDate;
    }

    public void setCancelledDate(Date cancelledDate) {
        this.cancelledDate = cancelledDate;
    }

    public Date getFulfilledDate() {
        return fulfilledDate;
    }

    public void setFulfilledDate(Date fulfilledDate) {
        this.fulfilledDate = fulfilledDate;
    }

    public Date getApprovedDate() {
        return approvedDate;
    }

    public void setApprovedDate(Date approvedDate) {
        this.approvedDate = approvedDate;
    }

    public Date getMappedDate() {
        return mappedDate;
    }

    public void setMappedDate(Date mappedDate) {
        this.mappedDate = mappedDate;
    }

    public Date getbFMApprovalDate() {
        return bFMApprovalDate;
    }

    public void setbFMApprovalDate(Date bFMApprovalDate) {
        this.bFMApprovalDate = bFMApprovalDate;
    }

    public int getCandidate() {
        return candidate;
    }

    public void setCandidate(int candidate) {
        this.candidate = candidate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isExtended() {
        return isExtended;
    }

    public void setExtended(boolean extended) {
        isExtended = extended;
    }

    public Date getRequestedOn() {
        return requestedOn;
    }

    public void setRequestedOn(Date requestedOn) {
        this.requestedOn = requestedOn;
    }

    public String getHiringStatus() {
        return hiringStatus;
    }

    public void setHiringStatus(String hiringStatus) {
        this.hiringStatus = hiringStatus;
    }

    public Date getUploadedOn() {
        return uploadedOn;
    }

    public void setUploadedOn(Date uploadedOn) {
        this.uploadedOn = uploadedOn;
    }

    public Date getStatusUpdatedOn() {
        return statusUpdatedOn;
    }

    public void setStatusUpdatedOn(Date statusUpdatedOn) {
        this.statusUpdatedOn = statusUpdatedOn;
    }

    public String getStatusUpdatedBy() {
        return statusUpdatedBy;
    }

    public void setStatusUpdatedBy(String statusUpdatedBy) {
        this.statusUpdatedBy = statusUpdatedBy;
    }

    public String getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    public boolean isGrouped() {
        return grouped;
    }

    public void setGrouped(boolean grouped) {
        this.grouped = grouped;
    }
}
