package com.ibs.irecruitment.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class DemandGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer jdGroupId;

    private String jdGroupName;
    private Date createdDate;
    private Date updatedOn;
    @OneToMany
    private List<Demand> demands;
    private String status;
    private String hiringStatus;

    public Integer getJdGroupId() {
        return jdGroupId;
    }

    public void setJdGroupId(Integer jdGroupId) {
        this.jdGroupId = jdGroupId;
    }

    public String getJdGroupName() {
        return jdGroupName;
    }

    public void setJdGroupName(String jdGroupName) {
        this.jdGroupName = jdGroupName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public List<Demand> getDemands() {
        return demands;
    }

    public void setDemands(List<Demand> demands) {
        this.demands = demands;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHiringStatus() {
        return hiringStatus;
    }

    public void setHiringStatus(String hiringStatus) {
        this.hiringStatus = hiringStatus;
    }
}
