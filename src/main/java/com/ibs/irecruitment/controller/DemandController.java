package com.ibs.irecruitment.controller;

import com.ibs.irecruitment.model.Demand;
import com.ibs.irecruitment.model.DemandGroup;
import com.ibs.irecruitment.model.RecruiterModel;
import com.ibs.irecruitment.service.interfaces.IDemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/openDemand/")
public class DemandController {

    @Autowired
    private IDemandService demandService;

    @PostMapping("csvUpload")
    public void uploadDemandExcel(@RequestParam("demand") MultipartFile uploadedFile) {
        demandService.parseDemandExcel(uploadedFile);
    }

    @GetMapping("demand")
    public ResponseEntity fetchAllDemands() {
        return ResponseEntity.ok(demandService.getAllDemands());
    }

    @GetMapping("activeDemands")
    public ResponseEntity fetchAllActiveDemands() {
        return ResponseEntity.ok(demandService.getAllActiveDemands());
    }

    @PostMapping("assign")
    public ResponseEntity assignDemand(@RequestBody RecruiterModel recruiterModel) {
        return ResponseEntity.ok(demandService.assignDemandToRecruiter(recruiterModel));
    }

    @PostMapping("recruiter")
    public ResponseEntity createRecruiter(@RequestBody List<RecruiterModel> recruiters) {
        return ResponseEntity.ok(demandService.createRecruiter(recruiters));
    }

    @GetMapping("recruiter")
    public ResponseEntity getAllRecruiter() {
        return ResponseEntity.ok(demandService.getAllRecruiters());
    }

    @GetMapping("recruiterWithDemand")
    public ResponseEntity getAllRecruiterWithDemand() {
        return ResponseEntity.ok(demandService.getAllRecruiterWithDemand());
    }

    @PostMapping("groupDemand")
    public ResponseEntity groupDemands(@RequestBody DemandGroup demandGroup) {
        return ResponseEntity.ok(demandService.groupDemands(demandGroup));
    }

    @PostMapping("updateGroupDemand")
    public ResponseEntity updateGroupDemands(@RequestBody DemandGroup demandGroup) {
        return ResponseEntity.ok(demandService.updateGroupDemands(demandGroup));
    }

    @GetMapping("groupDemands")
    public ResponseEntity getGroupedDemands(@RequestParam("status") String status) {
        return ResponseEntity.ok(demandService.getGroupedDemands(status));
    }

    @PostMapping("updateStatus")
    public ResponseEntity updateDemandStatus(@RequestBody List<Demand> demands) {
        return ResponseEntity.ok((demandService.updateDemandStatus(demands)));
    }

    @PostMapping("updateHiringStatus")
    public ResponseEntity updateHiringStatus(@RequestBody List<Demand> demands) {
        return ResponseEntity.ok(demandService.updateHiringStatus(demands));
    }
}
