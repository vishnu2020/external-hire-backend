package com.ibs.irecruitment.controller;

import com.ibs.irecruitment.model.JobModel;
import com.ibs.irecruitment.model.LocationModel;
import com.ibs.irecruitment.repository.LocationRepo;
import com.ibs.irecruitment.service.impl.JobModelServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*")
public class JobController {

    @Autowired
    private LocationRepo locationRepo;

    @Autowired
    private JobModelServiceImpl jobModelService;


    @GetMapping(value = "getLocation")
    public List<LocationModel> getAllLocation(){
        return (List<LocationModel>) locationRepo.findAll();
    }

    @PostMapping(value = "saveJobOpening")
    public ResponseEntity<Integer> saveJobOpening(@RequestBody JobModel jobModel){
        return ResponseEntity.ok(jobModelService.createNewJobPost(jobModel).getJobId());
    }

    @PostMapping(value = "updateJobOpening")
    public ResponseEntity<Integer> updateJobOpening(@RequestBody JobModel jobModel){
        return ResponseEntity.ok(jobModelService.updateJobPost(jobModel).getJobId());
    }

    @GetMapping(value = "getAllJobOpenings")
    public List<JobModel> getAllJobOpenings(){
        return jobModelService.getAllJobOpening();
    }

//    @PostMapping(value = "saveJobCategory")
//    public void saveJobCategory(){
//        String[] jobCategory = {"Assistant Manager - PMO",
//                "Associate Business Analyst",
//                "Associate Business Consultant",
//                "Bi-Lingual Co-ordinator",
//                "Business Analyst",
//                "Business Associate",
//                "Business Consultant",
//                "Consultant",
//                "Consultant - Creative Design",
//                "Executive - PMO",
//                "Group Test Manager",
//                "Lead - PMO",
//                "Lead Technical Architect",
//                "Manager PMO",
//                "Product Manager",
//                "Program Manager",
//                "Project Architect",
//                "Project Leader",
//                "Project Manager",
//                "SAP Business Consultant",
//                "Senior  Test Engineer",
//                "Senior Business Analyst",
//                "Senior Business Consultant",
//                "Senior Consultant - Creative Design",
//                "Senior Database Architect",
//                "Senior Database Consultant",
//                "Senior Executive - PMO",
//                "Senior InfoSec Consultant",
//                "Senior Performance Test Engineer",
//                "Senior Project Manager",
//                "Senior Service Delivery Manager",
//                "Senior Software Engineer",
//                "Senior Solution Architect",
//                "Senior Technical Architect",
//                "Senior Test  Engineer",
//                "Senior Test Analyst",
//                "Senior Test Engineer",
//                "Senior Test Lead",
//                "Senior UI Designer",
//                "Senior UI Developer",
//                "Service Delivery Manager",
//                "Software Engineer",
//                "Solution Architect",
//                "Technical Architect",
//                "Technical Lead",
//                "Test Analyst",
//                "Test Consultant",
//                "Test Engineer",
//                "Test Lead",
//                "Test Manager",
//                "UI Designer",
//                "Vice President & Head of Service Delivery - ITS",
//                "Technical Writer",
//                "Business Content Writer",
//                "Senior Test Manager",
//                "Manager - Product Documentation",
//                "Director - Account Management",
//                "Enterprise Architect",
//                "Data Strategist",
//                "Manager - Talent Management",
//                "Program Consultant",
//                "Senior Process Analyst",
//                "Database Architect",
//                "Senior Manager- Pre Sales",
//                "Vice President - IT Services",
//                "Vice President & Head of IV & V",
//                "Manager - Product Marketing & Industrial Relations",
//                "Principal Consultant",
//                "Senior Vice President & Head of Consulting & Domain Services",
//                "Director - Sales",
//                "Manager - Sales",
//                "Account Manager",
//                "Vice President & Head of Service Delivery - Consulting & Domain Services",
//                "Senior Manager – Product Management & Architecture",
//                "Senior Manager - Presales",
//                "General Manager and Head of Digital & Innovation",
//                "Head of PMO - Consulting & Domain Services",
//                "Senior Product Manager",
//                "Senior Program Manager",
//                "Performance Test Analyst",
//                "Group Manager - NFR Testing",
//                "Senior Performance Test Lead",
//                "Senior Associate - Sales",
//                "Senior Performance Test Analyst",
//                "Senior Performance Test Analyst",
//                "Senior Manager - Product Documentation",
//                "Senior Security Test Engineer",
//                "Senior Security Test Analyst",
//                "Associate Consultant",
//                "Security Test Analyst",
//                "Senior Performance Test Analyst",
//                "Performance Test Lead",
//                "Assistant Manager - Talent Management",
//                "Security Test Engineer",
//                "Senior Technical Writer",
//                "Performance Test Analyst",
//                "Senior Performance Test Lead",
//                "Performance Test Analyst",
//                "Senior Program Manager",
//                "Senior Associate - Sales",
//                "Head of Services Business – East Asia",
//                "General Manager - Service Delivery, CDS",
//                "Vice President & Head of Strategic Account Management",
//                "Senior Product Manager",
//                "General Manager - Service Delivery, CDS",
//                "Performance Test Architect",
//                "Engagement Manager",
//                "Manager - Enterprise Architecture",
//                "Senior Executive - Talent Management",
//                "Executive - Talent Management",
//                "Lead - Talent Management",
//                "Head- Talent Management",
//                "Executive-Production Support"
//        };
//        for(int i=0;i<jobCategory.length;i++){
//            JobCategory jc = new JobCategory();
//            jc.setCategory(jobCategory[i]);
//            jobCategoryRepo.save(jc);
//        }
//    }
}
