package com.ibs.irecruitment.controller;

import com.ibs.irecruitment.model.CandidateModel;
import com.ibs.irecruitment.service.interfaces.CandidateService;
import com.ibs.irecruitment.service.interfaces.ResumeService;
import com.ibs.irecruitment.utilities.ModifyXMLFile;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ibs.irecruitment.model.Demand;

@RestController
public class FileUploadController {

    @Autowired
    private ResumeService resumeService;

    @Autowired
    private CandidateService candidateService;

    @PostMapping("/upload")
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile uploadedFile) throws IOException {
        InputStream inputStream =  new BufferedInputStream(uploadedFile.getInputStream());
        Workbook workbook = new XSSFWorkbook(inputStream);

        Sheet sheet = workbook.getSheetAt(0);

        Map<Integer, List<String>> data = new HashMap<>();
        int i = 0;
        for (Row row : sheet) {
//            data.put(i, new ArrayList<>());
            for (Cell cell : row) {
//                switch (cell.getCellTypeEnum()) {
//                    case STRING: ... break;
//                    case NUMERIC: ... break;
//                    case BOOLEAN: ... break;
//                    case FORMULA: ... break;
//                    default: data.get(new Integer(i)).add(" ");
//                }
                System.out.println(cell.getStringCellValue());
            }
            i++;
        }

        return null;
    }

    @GetMapping("/xml")
    public void parseXML(){
        ModifyXMLFile.listXML();
    }

    @PostMapping("/uploadResumes")
    public ResponseEntity uploadResumes(@RequestParam("resumes[]") List<MultipartFile> multipartFiles){
        return ResponseEntity.ok(resumeService.parseResume(multipartFiles));
    }

    @PostMapping("/exportCandidateDemandExcel")
    public ResponseEntity exportCandidateDemandExcel(@RequestBody List<Demand> demands){

        byte[] excel = candidateService.exportCandidateDemandExcel(demands);

//        File fileEntity = new File("");
//
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        header.setContentLength(excel.length);
        header.set("Content-Disposition", "attachment; filename=candidate_demand_report.xlsx");
        header.set("filename", "candidate_demand_report.xlsx");
//
//        return ResponseEntity.ok();
        return new ResponseEntity(excel, header, HttpStatus.OK);
//        return null;
    }
}

