package com.ibs.irecruitment.controller;

import com.ibs.irecruitment.common.CandidateStatus;
import com.ibs.irecruitment.model.CandidateModel;
import com.ibs.irecruitment.service.interfaces.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/")
//@CrossOrigin(origins = "*")
public class CandidateController {

    @Autowired
    private CandidateService candidateService;

//    @PostMapping(value = "saveCandidate")
//    public ResponseEntity<Integer> saveCandidate(@RequestBody CandidateModel candidateModel){
//        return ResponseEntity.ok(candidateService.saveCandidate(candidateModel).getCandidateId());
//    }

    @RequestMapping(value = "saveCandidate", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseEntity<Integer> handleFileUpload(@ModelAttribute CandidateModel candidateModel ) {

        return ResponseEntity.ok(candidateService.saveCandidate(candidateModel).getCandidateId());
    }

    @PostMapping(value = "saveCandidates")
    public ResponseEntity saveCandidateList(@RequestBody List<CandidateModel> candidateModel){
        return ResponseEntity.ok(candidateService.saveCandidateList(candidateModel));
    }

    @GetMapping(value = "getAllCandidate/{status}")
    public List<CandidateModel> getAllCandidates(@RequestParam(required = false) List<String> experience,
                                                 @RequestParam(required = false) List<String> skills,
                                                 @RequestParam(required = false) List<String> qualification,
                                                 @PathVariable("status") String status){
        return candidateService.getAllCandidate(status, experience, skills, qualification);
    }

    @GetMapping(value = "getApplicationsByJobId/{jobId}")
    public List<CandidateModel> getApplicationsByJobId(@PathVariable("jobId") int jobId){
        return candidateService.getAllCandidateByJobId(jobId);
    }

    @PostMapping("mapCandidateWithDemnds")
    public ResponseEntity mapCandidateWithDemands(@RequestBody CandidateModel candidateModel) {
        return ResponseEntity.ok(candidateService.mapCandidateWithDemands(candidateModel));
    }

    // dummy API

    @PutMapping("updateStatusOfCandidate/{status}")
    public ResponseEntity updateCandidatesStatus(@PathVariable("status") String status){
        return ResponseEntity.ok(candidateService.updateCandidatesStatus(CandidateStatus.valueOf(status.toUpperCase()).getStatus()));
    }

    @PutMapping("mapCandidateToJob")
    public ResponseEntity mapCandidateToJob(@RequestParam Integer jobId, @RequestParam Integer candidateId) {
        return ResponseEntity.ok(candidateService.mapCandidateToJob(jobId, candidateId));
    }


}
