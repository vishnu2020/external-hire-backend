package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.Demand;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DemandRepo extends JpaRepository<Demand, Integer> {
    List<Demand> findByAssigned(boolean isAssigned);
    List<Demand> findByStatus(String status);
}
