package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.RecruiterModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecruiterRepo extends JpaRepository<RecruiterModel,String> {
}
