package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.JobModel;
import org.springframework.data.repository.CrudRepository;

public interface JobModelRepo extends CrudRepository<JobModel, Integer> {
}
