package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.CandidateModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CandidateRepo extends JpaRepository<CandidateModel, Integer>, CandidateRepoCustom {
    @Query("from CandidateModel where jobId =:jobId")
    public Iterable<CandidateModel> findByJobId(@Param("jobId") int jobId);

    @Query(value = "SELECT * FROM candidate_model cm where cm.experience = :experience", nativeQuery = true)
    List<CandidateModel> filterByParams(@Param("experience") List<String> experience);

    @Query(value=" select * from candidate_model ORDER BY candidate_id DESC LIMIT 0 , 1" , nativeQuery = true)
    public List<CandidateModel> findLastByOrderByCandidateIdDesc();

}
