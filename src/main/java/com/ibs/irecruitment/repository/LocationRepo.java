package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.LocationModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepo extends JpaRepository<LocationModel, String> {
}
