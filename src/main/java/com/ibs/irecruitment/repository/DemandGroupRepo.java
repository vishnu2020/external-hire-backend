package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.DemandGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DemandGroupRepo extends JpaRepository<DemandGroup, Integer> {
    List<DemandGroup> findByStatus(String status);

//    @Query(value = "SELECT demands FROM demand_group dmg where dmg.jdGroupId = :id", nativeQuery = true)
//    Object[] findByGroupId(@Param("id") int groupId);
}
