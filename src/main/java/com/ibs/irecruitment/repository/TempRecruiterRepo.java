package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.RecruiterModel;
import com.ibs.irecruitment.model.RecruiterModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.math.BigInteger;
import java.util.List;

public interface TempRecruiterRepo extends JpaRepository<RecruiterModel, Integer> {
    @Query(value = "SELECT id, emp_id, employee_name FROM EH_RECRUITER_MASTER ehr", nativeQuery = true)
    List<Object[]> findRecruiterWithoutDemand();
}
