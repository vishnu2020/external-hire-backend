package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.CandidateModel;
import org.apache.commons.collections4.functors.PredicateDecorator;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
class CandidateRepoImpl implements CandidateRepoCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<CandidateModel> getCandidateByApplyingFilter(String status, List<String> experiences, List<String> skills, List<String> qualifications) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<CandidateModel> criteriaQuery = criteriaBuilder.createQuery(CandidateModel.class);

        Root<CandidateModel> candidateModel = criteriaQuery.from(CandidateModel.class);
        CriteriaBuilder.In<String> experienceInClause = criteriaBuilder.in(candidateModel.get("experience"));
        CriteriaBuilder.In<String> skillsInClause = criteriaBuilder.in(candidateModel.get("skills"));
        CriteriaBuilder.In<String> qualificationInClause = criteriaBuilder.in(candidateModel.get("qualification"));
        Predicate statusPredicate = criteriaBuilder.equal(candidateModel.get("status"), status);

        criteriaQuery.select(candidateModel);
        List<Predicate> inClauseList = new ArrayList<>();
        inClauseList.add(statusPredicate);
        if(experiences != null){
            experiences.stream().forEach(experience -> experienceInClause.value(experience));
            inClauseList.add(criteriaBuilder.or(experienceInClause));
        }
        if(skills != null) {
            skills.stream().forEach(skill -> skillsInClause.value(skill));
            inClauseList.add(criteriaBuilder.or(skillsInClause));
        }
        if(qualifications != null) {
            qualifications.stream().forEach(qualification -> qualificationInClause.value(qualification));
            inClauseList.add(criteriaBuilder.or(qualificationInClause));
        }

        criteriaQuery.where(inClauseList.toArray(new Predicate[inClauseList.size()]));

        TypedQuery<CandidateModel> query = em.createQuery(criteriaQuery);

        List<CandidateModel> candidateModels = query.getResultList();
        candidateModels.forEach(candidateModel1 -> {
            candidateModel1.setDemands(null);
            candidateModel1.setJobModel(null);
        });
        return candidateModels;
    }

}