package com.ibs.irecruitment.repository;

import com.ibs.irecruitment.model.CandidateModel;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface CandidateRepoCustom {

    List<CandidateModel> getCandidateByApplyingFilter(String status, List<String> experiences, List<String> skills, List<String> qualifications);

}