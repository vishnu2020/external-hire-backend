package com.ibs.irecruitment.utilities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Filer;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.xmlbeans.ResourceLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

@Service
public class ModifyXMLFile {

	@Autowired
	private static ResourceLoader resourceLoader;


	public static void listXML() {

	   try {
		String filePath = "/Users/vigilvrapheal/Desktop/resumePortal/ats/job-posting-header.xml";
		File file = new File(filePath);
		if(!file.exists()){
			file.createNewFile();
		}
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(file.getAbsolutePath());
		   System.out.println(file.getAbsolutePath());

		// Get the root element
		Node jobPost = doc.getFirstChild();

		// Get the staff element , it may not working if tag has spaces, or
		// whatever weird characters in front...it's better to use
		// getElementsByTagName() to get it directly.
		// Node staff = company.getFirstChild();

		// Get the staff element by tag name directly
		Node staff = doc.getElementsByTagName("jobpost").item(0);

		// update staff attribute
		NamedNodeMap attr = staff.getAttributes();
		Node nodeAttr = attr.getNamedItem("id");
		nodeAttr.setTextContent("2");

		// append a new node to staff
		Element age = doc.createElement("age");
		age.appendChild(doc.createTextNode("28"));
		staff.appendChild(age);

		// loop the staff child node
		NodeList list = staff.getChildNodes();

		for (int i = 0; i < list.getLength(); i++) {

                   Node node = list.item(i);

		   // get the salary element, and update the value
		   if ("salary".equals(node.getNodeName())) {
			node.setTextContent("2000000");
		   }

                   //remove firstname
		   if ("firstname".equals(node.getNodeName())) {
			staff.removeChild(node);
		   }

		}

		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File(filePath));
		transformer.transform(source, result);

		System.out.println("Done");

	   } catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	   } catch (TransformerException tfe) {
		tfe.printStackTrace();
	   } catch (IOException ioe) {
		ioe.printStackTrace();
	   } catch (SAXException sae) {
		sae.printStackTrace();
	   }
	}

	public static void main(String[] args) {


	}

	/**
	 *
	 * @param reportName
	 * @return
	 */
	public Set<String> getHeadings(String reportName) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();

//			Resource resource = new ClassPathResource("classpath:reports_heading/candidate_demand_report.xml");
//			InputStream inputStream = resource.getInputStream();

//			String fileName = "config/sample.txt";
//			ClassLoader classLoader = getClass().getClassLoader();
//
//			File file = new File(classLoader.getResource(fileName).getFile());

//			File file = ResourceUtils.getFile("classpath:/reports_heading/candidate_demand_report.xml");
//			System.out.println("File Found : " + file.exists());
//			System.out.println(file.getAbsolutePath());

//			ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath*:candidate_demand_report.xml");

//			System.out.println(resourceFile.getFilename());

//			ClassPathResource resource = new ClassPathResource(
//					"reports_heading/candidate_demand_report.xml");
			File resource = new ClassPathResource("candidate_demand_report.xml").getFile();
			Document document = builder.parse(resource);
			document.getDocumentElement().normalize();
			Element root = document.getDocumentElement();
			System.out.println(root.getNodeName());
			NodeList nList = document.getElementsByTagName("CandidateDemandReport");
			System.out.println("============================");
			for (int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				System.out.println("");    //Just a separator
				if (node.getNodeType() == Node.ELEMENT_NODE)
				{
					//Print each employee's detail
					Element eElement = (Element) node;
					System.out.println("Employee id : "    + eElement.getAttribute("id"));
					System.out.println("First Name : "  + eElement.getElementsByTagName("firstName").item(0).getTextContent());
					System.out.println("Last Name : "   + eElement.getElementsByTagName("lastName").item(0).getTextContent());
					System.out.println("Location : "    + eElement.getElementsByTagName("location").item(0).getTextContent());
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}